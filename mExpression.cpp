#include "mExpression.h"

mExpression::mExpression(const std::string _expression) : expression(_expression) {
	isCorrectBrackets = true;
	isInt = true;

	for (int i = 0; i < expression.length(); i++) {
		if (expression[i] >= '(' && expression[i] <= '9' || expression[i] == ' ') {
			if (expression[i] == '.' || expression[i] == ',') { isInt = false; }
			isCalculated = true;
		}
		else {
			isCalculated = false;
			break;
		}
	}

}

const bool mExpression::getIsInt() const {
	return isInt;
}


const std::string mExpression::checkBrackets() {
	std::string bracketsResult = "";

	if (expression != "") {
		TStack<char> stackOfBrackets(expression.length());

		for (int i = 0; i < expression.length(); i++) {

			if (expression[i] == '(') {
				stackOfBrackets.push('(');
				bracketsResult += '(';
			}
			else if (expression[i] == ')') {

				if (stackOfBrackets.isEmpty()) {
					isCorrectBrackets = false;
					isCalculated = false;
					bracketsResult += " ->(";
					bracketsResult += ')';
				}
				else {
					stackOfBrackets.pop();
					bracketsResult += ')';
				}

			}
		}

		if (!stackOfBrackets.isEmpty()) {
			isCorrectBrackets = false;
			isCalculated = false;

			while (!stackOfBrackets.isEmpty()) {
				stackOfBrackets.pop();
				bracketsResult += " ->)";
			}
		}
	}
	else isCorrectBrackets = false;

	return bracketsResult;
}

inline const std::string mExpression::getExpression() const {
	return expression;
}

const std::string mExpression::createPostfix() {
	std::string resultPostfix = "";

	if (isCorrectBrackets) {
		TStack<char> operation(expression.length());

		for (int i = 0; i < expression.length(); i++) {

			switch (expression[i]) {
			case '(':
				operation.push(expression[i]); // ����� � ���� '('
				break;

			case ')':
				while (operation.getHeadElement() != '(') { resultPostfix += operation.pop(); } // ���������� ���� ��������� ����������� ������ �� '(' 
				if (resultPostfix[resultPostfix.length() - 1] != ' ') { resultPostfix += ' '; } // �������������� �������
				operation.pop(); // ������� '(' �� �����
				break;

			case '+':
				while (!operation.isEmpty() && operation.getHeadElement() != '(') { resultPostfix += operation.pop(); } // ���������� ���� ��������� ����������� ������ �� '(' ��� �� �������
				if (resultPostfix[resultPostfix.length() - 1] != ' ') { resultPostfix += ' '; } // �������������� �������
				operation.push(expression[i]); // ����� � ���� ������� �������� +
				break;

			case '-':
				while (!operation.isEmpty() && operation.getHeadElement() != '(') { resultPostfix += operation.pop(); } // ���������� ���� ��������� ����������� ������ �� '(' ��� �� �������
				if (resultPostfix[resultPostfix.length() - 1] != ' ') { resultPostfix += ' '; } // �������������� �������
				operation.push(expression[i]);  // ����� � ���� ������� �������� -
				break;

			case '*':
				if (operation.getHeadElement() == '/') {
					while (!operation.isEmpty() && operation.getHeadElement() != '(') { resultPostfix += operation.pop(); } // ���������� ���� ��������� ����������� ������ �� '(' ��� �� �������
				}
				if (resultPostfix[resultPostfix.length() - 1] != ' ') { resultPostfix += ' '; } // �������������� �������
				operation.push(expression[i]); // ����� � ���� ������� �������� *
				break;

			case '/':
				if (operation.getHeadElement() == '*') {
					while (!operation.isEmpty() && operation.getHeadElement() != '(') { resultPostfix += operation.pop(); } // ���������� ���� ��������� ����������� ������ �� '(' ��� �� �������
				}
				if (resultPostfix[resultPostfix.length() - 1] != ' ') { resultPostfix += ' '; } // �������������� �������
				operation.push(expression[i]); // ����� � ���� ������� �������� /
				break;

			case ' ':
				if (resultPostfix[resultPostfix.length() - 1] != ' ') { resultPostfix += ' '; } // �������������� �������
				break;

			default:
				resultPostfix += expression[i]; // ��������� � ����������� ������ ��� �������� , ��������� '0'-'9' , ',' , '.'
			}


		}

		if (!operation.isEmpty()) {
			while (!operation.isEmpty()) { resultPostfix += operation.pop(); } // ���������� ���� ��������� ����������� ������ �� ������� �����
		}

	}
	else resultPostfix = "Expression cannot be converted to postfix form!";


	return resultPostfix;
}
double mExpression::calculatedPostfixInt() {
	double result = NULL;

	if (isInt) {
		if (isCalculated && isCorrectBrackets) {
			std::string postfix = createPostfix();
			TStack<double> answer(postfix.length());

			double secondOperand = 0;
			double firstOperand = 0;

			for (int i = 0; i < postfix.length(); i++) {

				switch (postfix[i]) {

				case '+':
					answer.push(answer.pop() + answer.pop());
					break;

				case '-':
					answer.push(-answer.pop() + answer.pop());
					break;

				case '*':
					answer.push(answer.pop() * answer.pop());
					break;

				case '/':
					secondOperand = answer.pop();
					firstOperand = answer.pop();
					if (secondOperand == 0) { throw std::exception("Division by zero!"); }
					else { answer.push(firstOperand / secondOperand); }
					break;

				default:
					if (postfix[i] != ' ') {

						int counter = i;
						int number = postfix[counter++] - '0';

						while (postfix[counter] >= '0' && postfix[counter] <= '9') {
							i = counter;
							number *= 10;
							number += postfix[counter++] - '0';
						}
						answer.push(number);

					}

				}

			}
			result = answer.pop();
		}
	}

	return result;
}
double mExpression::calculatedPostfixDouble() {
	double result = NULL;

	if (!isInt) {
		if (isCalculated && isCorrectBrackets) {
			std::string postfix = createPostfix();
			TStack<double> answer(postfix.length());

			double secondOperand = 0;
			double firstOperand = 0;

			for (int i = 0; i < postfix.length(); i++) {

				switch (postfix[i]) {

				case '+':
					answer.push(answer.pop() + answer.pop());
					break;

				case '-':
					answer.push(-answer.pop() + answer.pop());
					break;

				case '*':
					answer.push(answer.pop() * answer.pop());
					break;

				case '/':
					secondOperand = answer.pop();
					firstOperand = answer.pop();
					if (secondOperand == 0) { throw std::exception("Division by zero!"); }
					else { answer.push(firstOperand / secondOperand); }
					break;

				default:
					if (postfix[i] != ' ') {

						int counter = i;
						double number = (double)postfix[counter++] - '0';

						while (postfix[counter] >= '0' && postfix[counter] <= '9' || postfix[counter] == 44 || postfix[counter] == 46) {
							i = counter;

							if (postfix[counter] == 44 || postfix[counter] == 46) {
								counter++;
								while (postfix[counter] >= '0' && postfix[counter] <= '9')
								{
									i = counter;
									double numberTwo = (double)postfix[counter++] - '0';
									number += numberTwo / 10;
								}
							}
							else {
								number *= 10;
								number += (double)postfix[counter++] - '0';
							}

						}
						answer.push(number);

					}

				}

			}
			result = answer.pop();
		}
	}

	return result;
}

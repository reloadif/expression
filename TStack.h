#pragma once

#include <exception>

const int MAXSIZE = 10;

template < class T >
class TStack {
	int head;
	int sizeArray;
	T* pointerToArray;

public:
	TStack();
	explicit TStack(const int _sizeArray);
	TStack(TStack& stack);

	~TStack();

	const int getSizeArray() const;
	const T getHeadElement() const;

	void push(const T element);
	const T pop();

	const bool isEmpty() const;
	const bool isFull() const;

};



template < class T >
TStack<T>::TStack() {
	head = 0;
	sizeArray = MAXSIZE;
	pointerToArray = new T[sizeArray];
}
template < class T >
TStack<T>::TStack(const int _sizeArray) {
	head = 0;
	sizeArray = _sizeArray;
	pointerToArray = new T[sizeArray];
}
template < class T >
TStack<T>::TStack(TStack& stack) {
	this->head = stack.head;
	this->sizeArray = stack.sizeArray;
	this->pointerToArray = new T[sizeArray];

	for (int i = 0; i < sizeArray; i++) {
		this->pointerToArray[i] = stack.pointerToArray[i];
	}

}


template < class T >
TStack<T>::~TStack() {
	if (pointerToArray != nullptr) {
		delete[] pointerToArray;
		pointerToArray = nullptr;
		head = -1;
		sizeArray = -1;
	}
}


template < class T >
const int TStack<T>::getSizeArray() const {
	return sizeArray;
}
template < class T >
const T TStack<T>::getHeadElement() const {
	int rightHead = head - 1;
	return pointerToArray[rightHead];
}

template < class T >
void TStack<T>::push(const T element) {
	if (!isFull()) {
		pointerToArray[head] = element;
		head++;
	}
	else throw std::exception("Stack is full!");
}
template < class T >
const T TStack<T>::pop() {
	if (!isEmpty()) {
		head--;
		return pointerToArray[head];
	}
	else throw std::exception("Stack is empty!");
}


template < class T >
const bool TStack<T>::isEmpty() const {
	return head == 0;
}
template < class T >
const bool TStack<T>::isFull() const {
	return head == sizeArray;
}